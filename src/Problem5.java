import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        String N;
        Scanner sc = new Scanner(System.in);
        
        while(true) {
            System.out.print("Please input : ");
            N = sc.nextLine();
            System.out.println(N.trim());

            if (N.trim().equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                break;
            }
        } 
    }
}
    
