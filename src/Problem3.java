import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        int num1 ,num2;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        num1 = sc.nextInt();
        
        System.out.print("Please input second number: ");
        num2 = sc.nextInt();

        if(num1>num2){
            System.out.println("Error");
        }else{
            for (int i=num1; i <=num2; i++){
                System.out.print(i+" ");
            }
        }
    }
    
}
