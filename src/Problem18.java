import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        int n = sc.nextInt();
        if(n==1){
            System.out.print("Please input number: ");
            int num = sc.nextInt();
            for (int i=1; i<=num; i++ ){
                for (int j=0;j<i; j++ ){
                    System.out.print("*");
                }
                System.out.println();
            }

        } else if (n==2){
            System.out.print("Please input number: ");
            int num = sc.nextInt();
            for (int i=num; i>0; i-- ){
                for (int j=0;j<i; j++ ){
                    System.out.print("*");
                }
                System.out.println();
            }

        } else if (n==3){
            System.out.print("Please input number: ");
            int num = sc.nextInt();
            for (int i=0; i<num; i++ ){
                for (int j=0;j<num; j++ ){
                    if(j>=i){
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }

        } else if (n==4){
            System.out.print("Please input number: ");
            int num = sc.nextInt();
            for (int i=4; i>=0; i-- ){
                for (int j=0;j<num; j++ ){
                    if(j>=i){
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }

        } else if (n==5){
            System.out.print("Bye bye!!!");
        } else {
            System.out.print("Error: Please input number between 1-5");
        }
        
    }
    
}
